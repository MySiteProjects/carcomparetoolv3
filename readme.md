# Car Compare Tool V3

Car Compare Tool is a [AngularJS], [Bootstrap], [Edmunds API] powered single-page-application that allow users to search the specs of the vecihle and compare the specs in the table side by side.

### Website

Car Compare Tool is hosted on `bibucket.io`.
https://carcomparetoolv2.bitbucket.io/#/

### Tech

Car Compare Tool uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Bootstrap] - great UI boilerplate for modern web apps
* [Gulp] - the streaming build system
* [Edmunds API] - api that provides vehicle data

### Gulp Build

Car Compare Tool requires [Node.js] and [Gulp] to build.

```sh
$ cd CarCompareToolV3
$ npm install
$ gulp
```

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


[Car Compare Tool]: <https://carcomparetoolv2.bitbucket.io/#/>
[Edmunds API]: <http://developer.edmunds.com/>
[Node.js]: <http://nodejs.org>
[Bootstrap]: <http://twitter.github.com/bootstrap/>
[AngularJS]: <http://angularjs.org>
[Gulp]: <http://gulpjs.com>
