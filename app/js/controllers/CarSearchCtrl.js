(function() {
    angular
        .module('app')
        .controller('CarSearchCtrl', CarSearchCtrl);

    CarSearchCtrl.$inject = ['$location', '$log', 'CarDataService', 'CommonRouteService'];

    function CarSearchCtrl($location, $log, cds, crs) {
        var vm = this;
        vm.carList = [];
        vm.matchBaseUrl = matchBaseUrl;
        vm.matchResultUrl = matchResultUrl;
        vm.matchCompareUrl = matchCompareUrl;
        vm.routeToResultView = routeToResultView;
        vm.routeToCompareView = routeToCompareView;
        vm.clearSelectedModel = clearSelectedModel;
        vm.clearSelectedYear = clearSelectedYear;
        vm.selectedMake = emptyVariable();
        vm.selectedModel = emptyVariable();
        vm.selectedYear = emptyVariable();
        vm.oldSelectedMake = '';
        vm.oldSelectedModel = '';

        activate();

        function activate() {
            return getCarList().then(function() {
                $log.info('Activated Base View');
            });
        }

        function getCarList() {
            return cds.getCarList().then(function(data){
                vm.carList = data.makes;
                return vm.carList;
            });
        }

        function matchBaseUrl() {
            return $location.url().toString() === '/';
        }

        function matchResultUrl() {
            var reg = /^\/(?:car\/(.+)\/(.+)\/(\d+)(\/(\d+))?)$/g;
            return reg.test($location.url().toString());
        }

        function matchCompareUrl() {
            return $location.url().toString() === '/car/compare';
        }

        function routeToResultView() {
            if (vm.selectedMake.niceName && vm.selectedModel.niceName && vm.selectedYear.year) {
                crs.routeToResultView(vm.selectedMake.niceName, vm.selectedModel.niceName, vm.selectedYear.year);
            }
        }

        function routeToCompareView() {
            crs.routeToCompareView();
        }

        function emptyVariable() {
            return;
        }

        function clearSelectedModel() {
            var makeName = (vm.selectedMake) ? vm.selectedMake.name : '';
            if (vm.oldSelectedMake !== makeName) {
                vm.selectedModel = emptyVariable();
                vm.selectedYear = emptyVariable();
                vm.oldSelectedMake = makeName;
            }
        }

        function clearSelectedYear() {
            var modelName = (vm.selectedModel) ? vm.selectedModel.name : '';
            if (vm.oldSelectedModel !== modelName) {
                vm.selectedYear = emptyVariable();
                vm.oldSelectedModel = modelName;
            }
        }
    }
})();
