(function() {
    angular
        .module('app')
        .controller('CarCompareCtrl', CarCompareCtrl);

    CarCompareCtrl.$inject = ['$scope', '$log', 'GridOptionsService', 'CarDataService', 'CarCompareDataService', 'CommonRouteService'];

    function CarCompareCtrl($scope, $log, GridOptionsService, CarDataService, CarCompareDataService, CommonRouteService) {
        var vm = this;
        vm.gridOptions = {};
        vm.candidateStyles = [];
        vm.selectedStyles = [];
        vm.getStyles = getStyles;
        vm.addStyles = addStyles;
        vm.isSidebarHidden = true;
        $scope.deleteGridRow = deleteGridRow;
        $scope.routeToDetail = routeToDetail;

        activate();

        function activate() {
            vm.gridOptions = GridOptionsService.getGridOptions();
            initCandidateStyles();
        }

        function initCandidateStyles() {
            var baseVm = $scope.$parent.baseVm;
            if (baseVm.selectedMake && baseVm.selectedModel && baseVm.selectedYear){
                getStyles(baseVm.selectedMake.niceName, baseVm.selectedModel.niceName, baseVm.selectedYear.year);
            }
        }

        function getStyles(make, model, year) {
            if (make && model && year) {
                CarDataService.getCarStyles(make, model, year).then(function(data){
                    if (data.styles){
                        vm.candidateStyles = data.styles;
                        CarDataService.getCarStylesExtra(make, model, year).then(function(extraData){
                            if (extraData.styleHolder){
                                var carStylesExtra = extraData.styleHolder;
                                angular.forEach(vm.candidateStyles, function(style){
                                    var tempAttributeGroups = _.find(carStylesExtra, { 'id': style.id }).attributeGroups;
                                    style.attributeGroups = tempAttributeGroups;
                                });
                            } else {
                                $log.warn('extra data for ' + make + ' ' + model + ' ' + year + ' does not exists');
                            }
                        });
                    }
                });
            }
        }

        function addStyles() {
            _.forEach(vm.selectedStyles, function(style){
                CarCompareDataService.addCarStyle(style);
            });

            vm.gridOptions.data = CarCompareDataService.getCarStyles();
            vm.selectedStyles = [];
        }

        function deleteGridRow(id) {
            CarCompareDataService.removeCarStyleById(id);
            vm.gridOptions.data = CarCompareDataService.getCarStyles();
        }

        function routeToDetail(id) {
            var targetStyle = _.find(CarCompareDataService.getCarStyles(), {id: id});
            CommonRouteService.routeToResultViewWithStyleId(targetStyle.make.niceName, targetStyle.model.niceName, targetStyle.year.year, id);
        }
    }
})();
