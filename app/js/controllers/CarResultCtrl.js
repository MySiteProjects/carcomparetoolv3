(function() {
    angular
        .module('app')
        .controller('CarResultCtrl', CarResultCtrl);

    CarResultCtrl.$inject = ['$anchorScroll', '$route', '$log', 'CarDataService', 'CarCompareDataService', 'carStylesPrepService', 'carStylesExtraPrepService', 'toastr'];

    function CarResultCtrl($anchorScroll, $route, $log, CarDataService, CarCompareDataService, carStylesPrepService, carStylesExtraPrepService, toastr) {
        var vm = this;
        vm.isSidebarHidden = true;
        vm.carStyles = [];
        vm.selectedStyle = {id: 0};
        vm.retrieveDetailInfo = retrieveDetailInfo;
        vm.isAlreadyInTable = isAlreadyInTable;
        vm.addCarStyleToTable = addCarStyleToTable;
        vm.removeCarStyleFromTable = removeCarStyleFromTable;
        vm.selectedStyle.carPhotos = [];
        vm.gotoAnchor = gotoAnchor;
        vm.carPhotosCarouselSettings = {
            active: 0,
            interval: 5000,
            noWrapSlides: false,
        };

        activate();

        // first three methods initiate basic data
        function activate(){
            if (carStylesPrepService.styles) {
                vm.carStyles = _.sortBy(carStylesPrepService.styles, 'name');
                combineExtraData();
                initSelectedStyle();
            }
        }

        function combineExtraData() {
            if (carStylesExtraPrepService) {
                var carStylesExtra = carStylesExtraPrepService.styleHolder;
                if (carStylesExtra) {
                    angular.forEach(vm.carStyles, function(style){
                        var tempAttributeGroups = _.find(carStylesExtra, { 'id': style.id }).attributeGroups;
                        style.attributeGroups = tempAttributeGroups;
                    });
                } else {
                    $log.warn('extra data does not exists');
                }
            }
        }

        function initSelectedStyle(){
            var styleId = $route.current.params.styleId;
            if (styleId) {
                vm.selectedStyle = _.find(vm.carStyles, {'id': parseInt(styleId)}) || vm.carStyles[0];
            } else {
                vm.selectedStyle = vm.carStyles[0];
            }
            retrieveDetailInfo();
        }


        // function triggered when changing style
        function retrieveDetailInfo() {
            getCarPhotos().then(function(data){ if (data) vm.selectedStyle.carPhotos = data; });
            getCarRatings().then(function(data){ if (data) vm.selectedStyle.ratings = data; })
            // more methods here;
        }

        function getCarPhotos() {
            return CarDataService.getCarPhotos(vm.selectedStyle.id);
        }

        function getCarRatings() {
            return CarDataService.getCarRatings(vm.selectedStyle.make.niceName, vm.selectedStyle.model.niceName, vm.selectedStyle.year.year, vm.selectedStyle.categories.vehicleStyle.toLowerCase());
        }


        //adding and removing style from table
        function isAlreadyInTable(id) {
            return CarCompareDataService.checkCarStyleExists(id);
        }

        function addCarStyleToTable() {
            CarCompareDataService.addCarStyle(vm.selectedStyle);
            var successToast = toastr.success('Add ' + vm.selectedStyle.trim + ' Successfully');
        }

        function removeCarStyleFromTable() {
            CarCompareDataService.removeCarStyle(vm.selectedStyle);
            var successToast = toastr.success('Remove ' + vm.selectedStyle.trim + ' Successfully');
        }

        function gotoAnchor(x) {
            $anchorScroll('anchor-' + x);
            vm.isSidebarHidden = !vm.isSidebarHidden;
        }
    }
})();
