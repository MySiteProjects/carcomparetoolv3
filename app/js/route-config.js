(function() {
    angular
        .module('app')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/templates/CarSearch.html',
            })
            .when('/car/:make/:model/:year', {
                templateUrl: '/templates/CarResult.html',
                controller: 'CarResultCtrl',
                controllerAs: 'vm',
                resolve: {
                    carStylesPrepService: carStylesPrepService,
                    carStylesExtraPrepService: carStylesExtraPrepService,
                }
            })
            .when('/car/:make/:model/:year/:styleId', {
                templateUrl: '/templates/CarResult.html',
                controller: 'CarResultCtrl',
                controllerAs: 'vm',
                resolve: {
                    carStylesPrepService: carStylesPrepService,
                    carStylesExtraPrepService: carStylesExtraPrepService,
                }
            })
            .when('/car/compare', {
                templateUrl: '/templates/CarCompare.html',
                controller: 'CarCompareCtrl',
                controllerAs: 'vm',
            })
            .otherwise({
                redirectTo: '/'
            });

        carStylesPrepService.$inject = ["CarDataService", "$route", "CommonRouteService"];

        function carStylesPrepService (cds, $route, CommonRouteService) {
            return cds.getCarStyles($route.current.params.make, $route.current.params.model, $route.current.params.year).then(function(data){
                if (data) return data;
                else return CommonRouteService.routeToResultView('mercedes-benz', 'c-class', '2015');
            });
        }

        carStylesExtraPrepService.$inject = ["CarDataService", "$route"];

        function carStylesExtraPrepService (cds, $route) {
            return cds.getCarStylesExtra($route.current.params.make, $route.current.params.model, $route.current.params.year);
        }
    }
})();
