(function() {
    angular
        .module('app')
        .config(config);

    config.$inject = ['toastrConfig'];

    function config(toastrConfig) {
        angular.extend(toastrConfig, {
            autoDismiss: false,
            closeButton: true,
            containerId: 'toast-container',
            extendedTimeOut: 1000,
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-bottom-center',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            tapToDismiss: true,
            target: 'body',
            timeOut: 1000,
        });
    }
})();
