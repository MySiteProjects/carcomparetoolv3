(function() {
    angular
        .module('app')
        .factory('CarCompareDataService', CarCompareDataService);

    CarCompareDataService.$inject = ['$log', 'CarDataService'];

    function CarCompareDataService($log, CarDataService) {
        var comparedCarStyles = [];

        var service = {
            getCarStyles: getCarStyles,
            checkCarStyleExists: checkCarStyleExists,
            addCarStyle: addCarStyle,
            removeCarStyle: removeCarStyle,
            removeCarStyleById: removeCarStyleById,
        };

        return service;

        function getCarStyles() {
            return comparedCarStyles;
        }

        function checkCarStyleExists(id) {
            return (_.findIndex(comparedCarStyles, {'id': id}) > -1) ? true : false;
        }

        function addCarStyle(style) {
            if (!checkCarStyleExists(style.id)) comparedCarStyles.push(style);
        }

        function removeCarStyle(style) {
            if (checkCarStyleExists(style.id)) comparedCarStyles = _.pullAllBy(comparedCarStyles, [{'id': style.id}], 'id');
        }

        function removeCarStyleById(id) {
            if (checkCarStyleExists(id)) comparedCarStyles = _.pullAllBy(comparedCarStyles, [{'id': id}], 'id');
        }
    }
})();
