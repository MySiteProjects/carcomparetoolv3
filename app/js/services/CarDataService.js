(function() {
    angular
        .module('app')
        .factory('CarDataService', CarDataService);

    CarDataService.$inject = ['$http', '$log'];

    function CarDataService($http, $log) {
        var service = {
            getCarList: getCarList,
            getCarStyles: getCarStyles,
            getCarStylesExtra: getCarStylesExtra,
            getCarPhotos: getCarPhotos,
            getCarRatings: getCarRatings,
            getConsumerCarRatings: getConsumerCarRatings,
            getCarSafety: getCarSafety,
        };

        return service;

        function getCarList() {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/api/vehicle/v2/makes",
                method: "GET",
                params: {
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    view: "full",
                    fmt: "json"
                }
            })
            .then(getCarListComplete)
            .catch(getCarListFailed);

            function getCarListComplete(response) {
                $log.log('XHR suceeded for getCarList.');
                return response.data;
            }

            function getCarListFailed(error) {
                $log.log('XHR failed for getCarList.' + error.data.makes);
                return;
            }
        }

        function getCarStyles(make, model, year) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/api/vehicle/v2/" + make + "/" + model +  "/" + year + "/styles",
                method: "GET",
                params: {
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    view: "full",
                    fmt: "json"

                }
            })
            .then(getCarStylesComplete)
            .catch(getCarStylesFailed);

            function getCarStylesComplete(response) {
                $log.log("XHR suceeded for getCarStyles.");
                return response.data;
            }

            function getCarStylesFailed(error) {
                $log.log('XHR failed for getCarStyles.');
                return;
            }
        }

        function getCarStylesExtra(make, model, year) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/v1/api/vehicle/stylerepository/findstylesbymakemodelyear",
                method: "GET",
                params: {
                    make: make,
                    model: model,
                    year: year,
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    fmt: "json"
                }
            })
            .then(getCarStylesComplete)
            .catch(getCarStylesFailed);

            function getCarStylesComplete(response) {
                $log.log("XHR suceeded for getCarStylesExtra.");
                return response.data;
            }

            function getCarStylesFailed(error) {
                $log.log('XHR failed for getCarStylesExtra.');
                return;
            }
        }

        function getCarPhotos(styleId) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid",
                method: "GET",
                params: {
                    styleId: styleId,
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    fmt: "json"
                }
            })
            .then(getCarPhotosComplete)
            .catch(getCarPhotosFailed);

            function getCarPhotosComplete(response) {
                var data = response.data;
                angular.forEach(data, function(d, i, arr){
                    angular.forEach(arr[i].photoSrcs, function(p, j, arr2){
                        var sizes = arr2[j].match(/\d+/g);
                        arr2[j] = {
                            src: 'https://media.ed.edmunds-media.com' + arr2[j],
                            size: parseInt(sizes[sizes.length-1]),
                        };
                    });
                    arr[i].photoSrcs = _.sortBy(arr[i].photoSrcs, 'size');
                    arr[i].photoSrcs = arr[i].photoSrcs[arr[i].photoSrcs.length-2];
                });
                $log.log("XHR suceeded for getCarPhotos.");
                return response.data;
            }

            function getCarPhotosFailed(error) {
                $log.log('XHR failed for getCarPhotos.');
                return;
            }
        }

        function getCarRatings(make, model, year, body) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/api/vehicle/v2/grade/" + make + "/" + model +  "/" + year,
                method: "GET",
                params: {
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    submodel: body,
                    fmt: "json"
                }
            })
            .then(getRatingsComplete)
            .catch(getRatingsFailed);

            function getRatingsComplete(response) {
                console.log("XHR suceeded for getCarRatings.");
                return response.data;
            }

            function getRatingsFailed(error) {
                console.log('XHR failed for getCarRatings.');
                return;
            }
        }

        function getConsumerCarRatings(make, model, year) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/api/vehicle/v2/grade/" + make + "/" + model +  "/" + year,
                method: "GET",
                params: {
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    fmt: "json"
                }
            })
            .then(getConsumerRatingsComplete)
            .catch(getConsumerRatingsFailed);

            function getConsumerRatingsComplete(response) {
                console.log("XHR suceeded for getCarRatings.");
                return response.data;
            }

            function getConsumerRatingsFailed(error) {
                console.log('XHR failed for getCarRatings.');
                return;
            }
        }

        function getCarSafety(make, model, year) {
            return $http({
                cache: true,
                url: "https://api.edmunds.com/api/vehicle/v2/" + make + "/" + model +  "/" + year + "/safety",
                method: "GET",
                params: {
                    api_key: "hgm3w37zfwmg7r3puwenyf5y",
                    view: "full",
                    fmt: "json"
                }
            })
            .then(getSafetyComplete)
            .catch(getSafetyFailed);

            function getSafetyComplete(response) {
                console.log("XHR suceeded for getCarSafety.");
                return response.data;
            }

            function getSafetyFailed(error) {
                console.log('XHR failed for getCarSafety.');
                return;
            }
        }
    }
})();
