(function() {
    angular
        .module('app')
        .factory('GridOptionsService', GridOptionsService);

    GridOptionsService.$inject = ['$log', 'CarCompareDataService'];

    function GridOptionsService($log, CarCompareDataService) {
        var gridOptions = {
            data: CarCompareDataService.getCarStyles(),
            enableColumnResizing: true,
            enableFiltering: true,
            enableGridMenu: true,
            enableSorting: true,
            enableHorizontalScrollbar: 1,
            enableVerticalScrollbar: 1,
            treeRowHeaderAlwaysVisible: false,
            pinnable: true,
            columnDefs: [
            { field: 'id', displayName: ' ', width: '50', enableFiltering: false, enableSorting: false,
                cellTemplate: '<button class="btn btn-primary btn-sm" ng-click="grid.appScope.routeToDetail(COL_FIELD)"><i class="fa fa-search fa-inverse"></i></button>' },
            { field: 'id', displayName: ' ', width: '50', enableFiltering: false, enableSorting: false,
                cellTemplate: '<button class="btn btn-danger btn-sm" ng-click="grid.appScope.deleteGridRow(COL_FIELD)"><i class="fa fa-remove fa-inverse"></i></button>' },
            { field: 'make.name', displayName: 'Make', width: '65' },
            { field: 'model.name', displayName: 'Model', width: '65' },
            { field: 'year.year', displayName: 'Year', width: '65' },
            { field: 'categories.vehicleStyle', displayName: 'Style', width: '65' },
            { field: 'trim', displayName: 'Trim', width: '125' },
            { field: 'name', displayName: 'Trim Name', width: '125' },
            { field: 'categories.market', displayName: 'Market Category', width: '125' },
            { field: 'categories.EPAClass', displayName: 'EPA Class', width: '125' },
            { field: 'attributeGroups.STYLE_INFO.attributes.TMV_CATEGORY.value', displayName: 'TMV Class', width: '125' },
            { field: 'attributeGroups.STYLE_INFO.attributes.WHERE_BUILT.value', displayName: 'Where Built', width: '125' },
            { field: 'attributeGroups.SPECIFICATIONS.attributes.CURB_WEIGHT.value', displayName: 'Curb Weight', width: '125' },
            { field: 'MPG.highway', displayName: 'EPA Highway', width: '125' },
            { field: 'MPG.city', displayName: 'EPA City', width: '125' },
            { field: 'attributeGroups.SPECIFICATIONS.attributes.EPA_COMBINED_MPG.value', displayName: 'EPA Combined', width: '125' },
            { field: 'attributeGroups.SPECIFICATIONS.attributes.TURNING_DIAMETER.value', displayName: 'Turning Diameter', width: '65' },
            { field: 'attributeGroups.SPECIFICATIONS.attributes["MANUFACTURER_0_60MPH_ACCELERATION_TIME_(SECONDS)"].value', displayName: '0-60 mph', width: '65' },
            { field: 'engine.type', displayName: 'Engine Type', width: '125' },
            { field: 'engine.size', displayName: 'Engine Size', width: '125' },
            { field: 'engine.configuration', displayName: 'Engine Configuration', width: '125' },
            { field: 'engine.cylinder', displayName: 'Num of Cylinders', width: '125' },
            { field: 'engine.horsepower', displayName: 'Horsepower', width: '125' },
            { field: 'engine.torque', displayName: 'Torque', width: '125' },
            { field: 'engine.compressorType', displayName: 'Compressor Type', width: '125' },
            { field: 'engine.compressionRatio', displayName: 'Comparession Ratio', width: '125' },
            { field: 'engine.totalValves', displayName: 'Num of Valves', width: '125' },
            { field: 'engine.valve.timing', displayName: 'Valve Timing', width: '125' },
            { field: 'engine.valve.gear', displayName: 'Valve Gear', width: '125' },
            { field: 'transmission.transmissionType', displayName: 'Transmission Type', width: '125' },
            { field: 'transmission.numberOfSpeeds', displayName: 'Num of Speeds', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.FRONT_SUSPENSION_CLASSIFICATION.value', displayName: 'Front Suspension Classification', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.FRONT_SPRING_TYPE.value', displayName: 'Front Spring Type', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.FRONT_STABILIZER_BAR.value', displayName: 'Front Stabilizer Bar', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.REAR_SUSPENSION_CLASSIFICATION.value', displayName: 'Rear Suspension Classification', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.REAR_SPRING_TYPE.value', displayName: 'Rear Spring Type', width: '125' },
            { field: 'attributeGroups.SUSPENSION.attributes.REAR_STABILIZER_BAR.value', displayName: 'Rear Stabilizer Bar', width: '125' },
            { field: 'attributeGroups.BRAKE_SYSTEM.attributes.FRONT_BRAKE_TYPE.value', displayName: 'Front Brake Type', width: '125' },
            { field: 'attributeGroups.BRAKE_SYSTEM.attributes.REAR_BRAKE_TYPE.value', displayName: 'Rear Brake Type', width: '125' },
            { field: 'attributeGroups.EXTERIOR_DIMENSIONS.attributes.OVERALL_HEIGHT.value', displayName: 'Exterior Overall Height', width: '125' },
            { field: 'attributeGroups.EXTERIOR_DIMENSIONS.attributes.OVERALL_LENGTH.value', displayName: 'Exterior Overall Length', width: '125' },
            { field: 'attributeGroups.EXTERIOR_DIMENSIONS.attributes.OVERALL_WIDTH_WITH_MIRRORS.value', displayName: 'Exterior Overall Width w/ Mirrors', width: '125' },
            { field: 'attributeGroups.EXTERIOR_DIMENSIONS.attributes.OVERALL_WIDTH_WITHOUT_MIRRORS.value', displayName: 'Exterior Overall Width w/o Mirrors', width: '125' },
            { field: 'attributeGroups.EXTERIOR_DIMENSIONS.attributes.WHEELBASE.value', displayName: 'Wheelbase', width: '125' },
            { field: 'attributeGroups.INTERIOR_DIMENSIONS.attributes["1ST_ROW_HEAD_ROOM"].value', displayName: '1st Row Head Room', width: '125' },
            { field: 'attributeGroups.INTERIOR_DIMENSIONS.attributes["1ST_ROW_LEG_ROOM"].value', displayName: '1st Row Leg Room', width: '125' },
            { field: 'attributeGroups.INTERIOR_DIMENSIONS.attributes["2ND_ROW_HEAD_ROOM"].value', displayName: '2nd Row Head Room', width: '125' },
            { field: 'attributeGroups.INTERIOR_DIMENSIONS.attributes["2ND_ROW_LEG_ROOM"].value', displayName: '2nd Row Leg Room', width: '125' },
            { field: 'attributeGroups.CARGO_DIMENSIONS.attributes["CARGO_CAPACITY,_ALL_SEATS_IN_PLACE"].value', displayName: 'Cargo Capacity', width: '125' },
            { field: 'options', displayName: 'Options', width: '125', enableFiltering: false, enableSorting: false, cellTemplate: '/templates/gridcell/optionField.html' },
            { field: 'colors', displayName: 'Colors', width: '125', enableFiltering: false, enableSorting: false, cellTemplate: '/templates/gridcell/colorField.html' },
            ],
        };

        var service = {
            getGridOptions: getGridOptions,
        };

        return service;

        function getGridOptions() {
            return gridOptions;
        }
    }
})();
