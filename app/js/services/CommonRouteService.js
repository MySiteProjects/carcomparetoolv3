(function() {
    angular
        .module('app')
        .factory('CommonRouteService', CommonRouteService);

    CommonRouteService.$inject = ['$location', '$log'];

    function CommonRouteService($location, $log) {
        var service = {
            routeToResultView: routeToResultView,
            routeToCompareView: routeToCompareView,
            routeToResultViewWithStyleId: routeToResultViewWithStyleId,
        };

        return service;

        function routeToResultView(make, model, year) {
            $location.path('/car/' + make + '/' + model + '/' + year);
        }

        function routeToResultViewWithStyleId(make, model, year, styleId) {
            $location.path('/car/' + make + '/' + model + '/' + year + '/' + styleId);
        }

        function routeToCompareView() {
            $location.path('/car/compare');
        }
    }
})();
