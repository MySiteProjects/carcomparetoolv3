(function() {
    angular
        .module('app', [
            'ngRoute',
            'ngAnimate',
            'ngSanitize',
            'ngTouch',
            'ui.bootstrap',
            'angular-toArrayFilter',
            'toastr',
            'ui.select',
            'ui.grid',
            'ui.grid.pinning',
            'ui.grid.grouping',
            'ui.grid.resizeColumns',
        ]);
})();
