(function() {
    angular
        .module('app')
        .directive('attributeSection', attributeSection);

    function attributeSection() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/attributeSection.html',
            scope: {
                attributes: '=',
            }
        };
    }
})();
