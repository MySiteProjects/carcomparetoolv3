(function() {
    angular
        .module('app')
        .directive('setClassWhenAtTop', setClassWhenAtTop);

    setClassWhenAtTop.$inject = ['$window'];

    function setClassWhenAtTop($window) {
        var $win = angular.element($window); // wrap window object as jQuery object
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = parseInt(element[0].offsetTop) + parseInt(element[0].offsetHeight); // get element's offset top relative to document

                $win.on('scroll', function (e) {
                    element[($win[0].pageYOffset >= offsetTop) ? 'addClass' : 'removeClass'](topClass);
                });
            }
        };
    }
})();
