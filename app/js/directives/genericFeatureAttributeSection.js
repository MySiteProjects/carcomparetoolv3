(function() {
    angular
        .module('app')
        .directive('genericFeatureAttributeSection', genericFeatureAttributeSection);

    function genericFeatureAttributeSection() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/genericFeatureAttributeSection.html',
            scope: {
                attributes: '=',
            }
        };
    }
})();
