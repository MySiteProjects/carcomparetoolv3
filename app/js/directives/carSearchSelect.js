(function() {
    angular
        .module('app')
        .directive('carSearchSelect', carSearchSelect);

    function carSearchSelect() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/carSearchSelect.html',
        };
    }
})();
