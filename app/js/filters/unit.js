(function() {
    angular
        .module('app')
        .filter('unit', unit);

    function unit() {
        return function (input, p) {
            if (!isNaN(input)) {
                var txt = _.lowerCase(p.name),
                unit = "",
                matchingWords = ["weight", "mpg", "fuel capacity", "seconds",
                                 "turning diameter", "height", "length",
                                 "width", "wheelbase", "room", "cargo capacity",
                                 "seating capacity"],
                re = new RegExp(matchingWords.join("|"), "i"),
                match = txt.match(re) || [""];

                switch(match[0]){
                    case matchingWords[0]:
                        unit = "lbs.";
                        break;
                    case matchingWords[1]:
                        unit = "mpg";
                        break;
                    case matchingWords[2]:
                        unit = "gal.";
                        break;
                    case matchingWords[3]:
                        unit = "s";
                        break;
                    case matchingWords[4]:
                    case matchingWords[5]:
                    case matchingWords[6]:
                    case matchingWords[7]:
                    case matchingWords[8]:
                    case matchingWords[9]:
                        unit = "in.";
                        break;
                    case matchingWords[10]:
                    case matchingWords[11]:
                    case matchingWords[12]:
                        unit = "cu.-ft.";
                        break;
                    default:
                        unit = "";
                }

                return input + " " + unit;
            }
            else {
                return input;
            }
        };
    }
})();
