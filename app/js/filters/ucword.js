(function() {
    angular
        .module('app')
        .filter('ucword', ucword);

    function ucword() {
        return function (input) {
            if (isNaN(input))
                return _.startCase(_.lowerCase(input));
            else
                return input;
        };
    }
})();
