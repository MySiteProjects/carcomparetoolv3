(function() {
    angular
        .module('app')
        .filter('notInArray', notInArray);

    notInArray.$inject = ['$filter'];

    function notInArray($filter) {
        return function(list, arrayFilter, element) {
            if (arrayFilter) {
                return $filter("filter")(list, function(listItem) {
                    return _.findIndex(arrayFilter, function(o){return o[element] === listItem[element]}) == -1;
                });
            }
        };
    }
})();
