var gulp = require('gulp');
var gulpIf = require('gulp-if');
var uglify = require('gulp-uglify');
var useref = require('gulp-useref');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');

gulp.task('useref', function(){
    return gulp.src('app/**/*.html')
        .pipe(useref())
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulp.dest('dist/'));
});

gulp.task('images', function(){
    return gulp.src('app/assets/images/**/*.+(png|jpg|gif|svg|ico)')
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('clean:dist', function() {
  return del.sync('dist');
});

gulp.task('default', function (callback) {
    runSequence('clean:dist',
                ['useref', 'images'],
                callback);
});
